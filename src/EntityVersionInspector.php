<?php

namespace Drupal\admin_toolbar_entity_version;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workflows\WorkflowTypeInterface;

final class EntityVersionInspector {

  use StringTranslationTrait;

  const VERSION_DEFAULT = 'default';
  const VERSION_LATEST = 'latest';
  const VERSION_REVISION = 'revision';

  private EntityRepositoryInterface $entityRepository;

  private ?ModerationInformationInterface $moderationInformation;

  private DateFormatterInterface $dateFormatter;

  public function __construct(
    EntityRepositoryInterface $entity_repository,
    ?ModerationInformationInterface $moderation_information,
    DateFormatterInterface $date_formatter,
  ) {
    $this->entityRepository = $entity_repository;
    $this->moderationInformation = $moderation_information;
    $this->dateFormatter = $date_formatter;
  }

  public function getCurrentVersion(RevisionableInterface $entity): string {
    if ($entity->isDefaultRevision()) {
      return self::VERSION_DEFAULT;
    }
    if ($entity->isLatestRevision()) {
      return self::VERSION_LATEST;
    }
    return self::VERSION_REVISION;
  }

  public function getVersions(RevisionableInterface $entity): array {
    $default = $this->getDefaultVersion($entity);
    $latest = $this->getLatestVersion($entity);
    $workflow = $this->getWorkflow($entity);

    $versions = [];
    $versions[self::VERSION_DEFAULT] = $this->buildVersion(
      $default,
      $workflow,
      'canonical',
      $this->t('Canonical')
    );

    if ($latest->getRevisionId() !== $default->getRevisionId()) {
      $versions[self::VERSION_LATEST] = $this->buildVersion(
        $latest,
        $workflow,
        'latest-version',
        $this->t('Latest revision')
      );
    }

    if (!$entity->isDefaultRevision() && !$entity->isLatestRevision()) {
      $versions[self::VERSION_REVISION] = $this->buildVersion(
        $entity,
        $workflow,
        'revision',
        $this->t('Old revision')
      );
    }

    return $versions;
  }

  private function buildVersion(RevisionableInterface $entity, ?WorkflowTypeInterface $workflow, string $rel, string $label): array {
    $created = $this->getVersionCreated($entity);

    return [
      'label' => $label,
      'url' => $entity->hasLinkTemplate($rel) ? $entity->toUrl($rel) : NULL,
      'published' => $entity instanceof EntityPublishedInterface ? $entity->isPublished() : TRUE,
      'status' => $this->getVersionStatus($entity, $workflow),
      'created' => $created,
      'created_time_ago' => $created ? $this->dateFormatter->formatTimeDiffSince($created) : NULL,
    ];
  }

  private function getDefaultVersion(RevisionableInterface $entity): RevisionableInterface {
    $default = $entity->isDefaultRevision()
      ? $entity
      : $this->entityRepository->getCanonical($entity->getEntityTypeId(), $entity->id());
    assert($default instanceof RevisionableInterface);
    return $default;
  }

  private function getLatestVersion(RevisionableInterface $entity): RevisionableInterface {
    $latest = $entity->isLatestRevision()
      ? $entity
      : $this->entityRepository->getActive($entity->getEntityTypeId(), $entity->id());
    assert($latest instanceof RevisionableInterface);
    return $latest;
  }

  private function getWorkflow(RevisionableInterface $entity): ?WorkflowTypeInterface {
    if (!$entity instanceof ContentEntityInterface
      || !$this->moderationInformation
      || !$this->moderationInformation->isModeratedEntity($entity)
    ) {
      return NULL;
    }

    return $this->moderationInformation->getWorkflowForEntity($entity)->getTypePlugin();
  }

  private function getVersionStatus(RevisionableInterface $entity, ?WorkflowTypeInterface $workflow): string {
    if ($workflow) {
      assert($entity instanceof ContentEntityInterface);
      $state_id = $entity->get('moderation_state')->value;
      $state = $workflow->getState($state_id);
      return $state->label();
    }

    if ($entity instanceof EntityPublishedInterface) {
      return $entity->isPublished()
        ? $this->t('Published')
        : $this->t('Unpublished');
    }

    if ($entity->isDefaultRevision()) {
      return $this->t('Published');
    }

    if ($entity->isLatestRevision()) {
      return $this->t('Latest');
    }

    return $this->t('Revision');
  }

  private function getVersionCreated(RevisionableInterface $entity): ?int {
    if ($entity instanceof RevisionLogInterface) {
      return $entity->getRevisionCreationTime();
    }

    return NULL;
  }

}

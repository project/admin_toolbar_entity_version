<?php

namespace Drupal\admin_toolbar_entity_version;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class AdminToolbarEntityVersionBuilder implements ContainerInjectionInterface, TrustedCallbackInterface {

  private RouteMatchInterface $routeMatch;

  private EntityTypeManagerInterface $entityTypeManager;

  private EntityVersionInspector $entityVersionInspector;

  public function __construct(
    RouteMatchInterface $route_match,
    EntityTypeManagerInterface $entity_type_manager,
    EntityVersionInspector $entity_version_inspector,
  ) {
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityVersionInspector = $entity_version_inspector;
  }

  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('admin_toolbar_entity_version.inspector'),
    );
  }

  public static function trustedCallbacks(): array {
    return ['build'];
  }

  public function build(): array {
    $entity = $this->getEntity();
    if (!$entity) {
      return [];
    }

    $version_history_url = $entity->hasLinkTemplate('version-history') ? $entity->toUrl('version-history') : NULL;
    if ($version_history_url instanceof Url && !$version_history_url->access()) {
      $version_history_url = NULL;
    }

    return [
      '#theme' => 'admin_toolbar_entity_version',
      '#current_version' => $this->entityVersionInspector->getCurrentVersion($entity),
      '#versions' => $this->entityVersionInspector->getVersions($entity),
      '#version_history_url' => $version_history_url,
    ];
  }

  private function getEntity(): ?RevisionableInterface {
    if (!$this->routeMatch->getRouteName() || !preg_match('/^entity\.(.*)\.(canonical|latest_version|revision)$/', $this->routeMatch->getRouteName(), $matches)) {
      return NULL;
    }

    $entity_type_id = $matches[1];
    $rel = $matches[2];

    $entity_revision = $rel === 'revision'
      ? $this->routeMatch->getParameter("{$entity_type_id}_revision")
      : NULL;

    if ($entity_revision) {
      if ($entity_revision instanceof RevisionableInterface) {
        $entity = $entity_revision;
      }
      else {
        // @phpstan-ignore-next-line
        $entity = $this->entityTypeManager->getStorage($entity_type_id)
          ->loadRevision($entity_revision);
      }
    }
    else {
      $entity = $this->routeMatch->getParameter($entity_type_id);
    }

    return $entity instanceof RevisionableInterface
      ? $entity
      : NULL;
  }

}
